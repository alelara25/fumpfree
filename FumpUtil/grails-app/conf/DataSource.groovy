dataSource {                                                                                                                                                                                                                                 
    pooled = true                                                                                                                                                                                                                            
    logSql: true                                                                                                                                                                                                                             
}                                                                                                                                                                                                                                            
hibernate {                                                                                                                                                                                                                                  
    cache.use_second_level_cache = true                                                                                                                                                                                                      
    cache.use_query_cache = false                                                                                                                                                                                                            
//    cache.region.factory_class = 'org.hibernate.cache.SingletonEhCacheRegionFactory' // Hibernate 3                                                                                                                                        
    cache.region.factory_class = 'org.hibernate.cache.ehcache.SingletonEhCacheRegionFactory' // Hibernate 4                                                                                                                                  
    singleSession = true // configure OSIV singleSession mode                                                                                                                                                                                
    flush.mode = 'manual' // OSIV session flush mode outside of transactional context                                                                                                                                                        
}                                                                                                                                                                                                                                            
                                                                                                                                                                                                                                             
// environment specific settings                                                                                                                                                                                                             
environments {                                                                                                                                                                                                                               
    development {                                                                                                                                                                                                                            
        dataSource {                                                                                                                                                                                                                         
            dialect = "org.hibernate.dialect.PostgreSQLDialect"                                                                                                                                                                              
            driverClassName = "org.postgresql.Driver"                                                                                                                                                                                        
            username = "fump"                                                                                                                                                                                                          
            //password = "s1fr3m17%"                                                                                                                                                                                                         
            password = "fump20%"
            dbCreate = "update" // one of 'create', 'create-drop', 'update', 'validate', ''
            url = "jdbc:postgresql://10.8.54.33:5432/fump"

            //url = "jdbc:postgresql://10.8.6.144:5432/sirwebprod"//prod
            properties {
                maxActive = 25
                maxIdle = 25
                minIdle = 3
                initialSize = 3
                validationQuery = "select now()"
                minEvictableIdleTimeMillis = 60000
                timeBetweenEvictionRunsMillis = 120000
                maxWait = 2000
                removeAbandoned = true
                logAbandoned = true
                testOnBorrow = true
                testWhileIdle = true
                testOnReturn = true
            }


            }

    }
    test {
        dataSource {    
            dialect = "org.hibernate.dialect.PostgreSQLDialect"
            driverClassName = "org.postgresql.Driver"
            username = "sirwebprod"
            password = "sirwebprod"
            dbCreate = "update"
            dbCreate = "update" // one of 'create', 'create-drop', 'update', 'validate', ''
            url = "jdbc:postgresql://52.35.224.53:5432/sirwebprod"
        }   

    }
    production {
    dataSource {    
            dialect = "org.hibernate.dialect.PostgreSQLDialect"
            driverClassName = "org.postgresql.Driver"
            username = "sirwebprod"
            password = "s1fr3m17%"
            dbCreate = "update"
            //dbCreate = "update" // one of 'create', 'create-drop', 'update', 'validate', ''
            //url = "jdbc:postgresql://10.8.63.31:5432/sirwebprod"
            url = "jdbc:postgresql://10.8.63.29:5432/sirwebprod"
            properties {
                maxActive = 25
                maxIdle = 25
                minIdle = 3
                initialSize = 3
                validationQuery = "select now()"
                minEvictableIdleTimeMillis = 60000
                timeBetweenEvictionRunsMillis = 120000
                maxWait = 2000
                removeAbandoned = true
                logAbandoned = true
                testOnBorrow = true
                testWhileIdle = true
                testOnReturn = true
            }
        }   

    }
}
